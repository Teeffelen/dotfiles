# Dotfiles

Personal config files, used on most of my PCs.
Based on a Fedora Workstation installation running Gnome Desktop.

## Gnome desktop

Apply all preferences by running the script:

```bash
./setup-gnome.sh
```

## Flatpaks

To update the Flatpak application list, run the following:

```bash
flatpak list --app --columns=application >| flatpaks.list
```

## Dotfiles script

```bash
NAME
	dotfiles - file copy script for the dotfiles repository
SYNOPSIS
	./dotfiles.sh <command> [<args>]
COMMANDS
	list
		Print all files added to the dotfiles directory.
	get <filepath>
		Add file to the dotfiles directory.
	pull
		For each file in the dotfiles directory, copy that file on the system to
		the dotfiles directory.
	push
		For each file in the dotfiles directory, copy that file to its location
		on the system.
	packages [list]
		List all the packages installed on the system.
	packages store
		Store the list of all the installed packages on the system.
	packages install
		Install all the core packages of the stored list.
```
