# ~/.bashrc.d/aliases.sh

# Colors
alias ls="ls --color=auto --group-directories-first"
alias grep="grep --colour=auto"
alias egrep="egrep --colour=auto"
alias fgrep="fgrep --colour=auto"
alias diff="diff --color"

# General usage
alias sudo="sudo "
alias ll="ls -lGh"
alias lla="ll -A"
alias lsa="ls -A"
alias cp="cp -i"
alias rm="rm -i"
alias mv="mv -i"
alias mkdir="mkdir -p"
alias code="codium"

# Git
alias ga="git add"
alias gs="git status"
alias gc="git commit"
alias gcm="git commit -m"
alias gfa="git fetch --all"
alias gfp="git fetch --prune"
alias gpl="git pull"
alias gps="git push"
alias gd="git diff"
alias gdc="gd --cached"
alias gl="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d    %C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"

