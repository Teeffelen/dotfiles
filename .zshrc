# ~/.zshrc

# Disable CTRL+S and CTRL+Q inputs in terminal
stty -ixon

# Enable emacs bindings (instead of vim)
bindkey -e

# Change prompt style
PS1="%B%F{green}%n%f %B%1~% %(?.. %F{red}%?) %F{green}λ%f%b "

# Set zsh options (man zshoptions)
setopt NO_CLOBBER
setopt SHARE_HISTORY
setopt APPEND_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS

# Setup history file under ~/.cache
[[ -d "${HOME}/.cache/zsh" ]] || mkdir --parents "${HOME}/.cache/zsh"
HISTFILE="${HOME}/.cache/zsh/history"
HISTSIZE=5000
SAVEHIST=5000

# Enable auto completion on TAB, with menu selection
autoload -Uz compinit
compinit -d "${HOME}/.cache/zsh/zcompdump"
zstyle ':completion:*' menu select

# Keybindings (bindkey -L)
bindkey "^[[H"  beginning-of-line               # Home
bindkey "^[[F"  end-of-line                     # End
bindkey "^[[3~" delete-char                     # Delete
bindkey "^[[2~" overwrite-mode                  # Insert
bindkey "^?"    backward-delete-char            # Backspace
bindkey "^[[A"  up-line-or-history              # Up
bindkey "^[[B"  down-line-or-history            # Down
bindkey "^[[C"  forward-char                    # Right
bindkey "^[[D"  backward-char                   # Left
bindkey "^[[5~" beginning-of-buffer-or-history  # PgUp
bindkey "^[[6~" end-of-buffer-or-history        # PgDn
bindkey "^ "    autosuggest-accept              # Ctrl+Space
bindkey ";5D"   backward-word                   # Ctrl+Left
bindkey ";5C"   forward-word                    # Ctrl+Right
bindkey "^H"    backward-delete-word            # Ctrl+Backspace

# User specific aliases and functions
if [[ -d ~/.bashrc.d ]]; then
    for rc in ~/.bashrc.d/*; do
        [[ -f "${rc}" ]] && source "${rc}"
    done
fi
unset rc

# Load ZSH plugins, keep this at the end of your .zshrc file!
plugins=(
    "/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
    "/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
)
for plugin in "${plugins[@]}"; do
    [[ -f "${plugin}" ]] && source "${plugin}"
done
unset plugins plugin

